#pragma once
template <typename T, typename K> //waga, wartosc
class Dijkstra {
	T * distance;
	Vertex<T, K> * source;
	int verticesCount;
	Graph<T, K> graph;

public:
	Dijkstra(Graph<T,K> _graph, Vertex<T, K> * source) {
		graph = _graph;
		verticesCount = graph.vertices().size();
		distance = new T[verticesCount];
		for (int i = 0; i < verticesCount; i++)
			distance[i] = std::numeric_limits<T>::max();
		distance[source->getID()] = 0;
	}
	~Dijkstra() {
		//delete[] distance;
		source = nullptr;
	}
	void launch() {
		std::priority_queue<Vertex<T, K> *> Q;
		std::vector<Vertex<T, K> *>::iterator it;
		//for (it = graph.vertices().begin(); it != graph.vertices().end(); it++) {
		for(int i = 0; i < graph.vertices().size(); i++){
			//Q.push(*it);
			Q.push(graph.vertices()[i]);
		}
		while (!Q.empty()) {
			Vertex<T, K> * u = Q.top();
			Q.pop();
			for (int i = 0; i < graph.vertices().size(); i++) {
			//for (it = graph.vertices().begin(); it != graph.vertices().end(); it++) {
				//if (graph.areAdjancent(*it, u)) {
				if (graph.areAdjancent(graph.vertices()[i], u)) {
					if (distance[(graph.vertices()[i])->getID()] > distance[u->getID()] + graph.weight(graph.vertices()[i], u)) {
						distance[(graph.vertices()[i])->getID()] = distance[u->getID()] + graph.weight(graph.vertices()[i], u);
						Q.push(graph.vertices()[i]);
					}
				}
			}
		}
	}
};
