#pragma once
#include "Vertex.h"
template <typename T, typename K>
class Edge {
	T weight;
	Vertex<T, K> * v1;
	Vertex<T, K> * v2;



	bool isDirected;

public:
	std::list<Edge<T, K> *> * neighbours1;
	std::list<Edge<T, K> *> * neighbours2;
	Edge() {

	}
	Edge(Vertex<T, K> * _v1, Vertex<T, K> * _v2, T _weight) {
		v1 = _v1;
		v2 = _v2;
		weight = _weight;
		neighbours1 = _v1->getNeighbours();
		neighbours1 = _v2->getNeighbours();
	}

	~Edge() {
		v1 = nullptr;
		v2 = nullptr;
		neighbours1 = nullptr;
		neighbours2 = nullptr;
	}

	void setWeight(T _weight) {
		weight = _weight;
	}
	T getWeight() {
		return weight;
	}

	Vertex<T, K> * getV1() {
		return v1;
	}
	Vertex<T, K> * getV2() {
		return v2;
	}
};