#pragma once
#include "Edge.h"
#include "Vertex.h"
#include <list>
#include <vector>
#include <iterator>

template <typename T, typename K>
class Graph {
	std::list<Edge<T, K> *> edgesL;
	std::vector<Vertex<T, K> *> verticesL;

	T ** neighborhoodMatrix;

public:

	Graph() {
		neighborhoodMatrix = nullptr;
	}

	//endVertices(Edge<T, K> e)
	Vertex<T, K> * opposite(Vertex<T, K> * v, Edge<T, K> * e);
	bool areAdjancent(Vertex<T, K> * v, Vertex<T, K> * w);
	void replace(Vertex<T, K> * v, K x);
	void replace(Edge<T, K> * e, T x);

	Vertex<T, K> * insertVertex(K o);
	Edge<T, K> * insertEdge(Vertex<T, K> *v, Vertex<T, K> *w, T o);
	K removeVertex(Vertex<T, K> * v);
	T removeEdge(Edge<T, K> * e);

	std::list<Edge<T, K> *> * incidentEdges(Vertex<T, K> * v);
	std::vector<Vertex<T, K> *> vertices();
	std::list<Edge<T, K> *>& edges();

	T weight(Vertex<T, K> *v, Vertex<T, K> *u) {
		std::list<Edge<T, K> *>::iterator it;
		for (it = edgesL.begin(); it != edgesL.end(); it++) {
			if (((*it)->getV1() == v && (*it)->getV2() == u) || ((*it)->getV2() == v && (*it)->getV1() == u)) {
				return (*it)->getWeight();
			}
		}
		return 0;
	}
};

template <typename T, typename K>
Vertex<T, K> * Graph<T, K>::insertVertex(K o) {
	Vertex<T, K> * vertex = new Vertex<T, K>(o, verticesL.size()+1);
	verticesL.push_back(vertex);
	return vertex;
}

template <typename T, typename K>
Edge<T, K> * Graph<T, K>::insertEdge(Vertex<T, K> *v, Vertex<T, K> *w, T o) {
	Edge<T, K> * edge = new Edge<T, K>(v, w, o);
	edgesL.push_back(edge);

	v->getNeighbours()->push_back(edge);
	w->getNeighbours()->push_back(edge);
	return edge;
}

template <typename T, typename K>
std::vector<Vertex<T, K> *> Graph<T, K>::vertices() {
	return verticesL;
}

template <typename T, typename K>
std::list<Edge<T, K> *>& Graph<T, K>::edges() {
	return edgesL;
}

template <typename T, typename K>
void Graph<T, K>::replace(Vertex<T, K> * v, K x) {
	v->setData(x);
}

template <typename T, typename K>
void Graph<T, K>::replace(Edge<T, K> * e, T x) {
	e->setWeight(x);
}

template <typename T, typename K>
std::list<Edge<T, K> *> * Graph<T, K>::incidentEdges(Vertex<T, K> * v) {
	return v->getNeighbours();
}

template <typename T, typename K>
Vertex<T, K> * Graph<T, K>::opposite(Vertex<T, K> * v, Edge<T, K> * e) {
	if (e->getV1() == v) return e->getV2(); else return e->getV1();
}

template <typename T, typename K>
bool Graph<T, K>::areAdjancent(Vertex<T, K> * v, Vertex<T, K> * w) {
	std::list<Edge<T, K> *>::iterator it;
	for (it = v->neighbours->begin(); it != v->neighbours->end(); it++) {
		if ((*it)->getV1() == w) return true;
		if ((*it)->getV2() == w) return true;
	}
	return false;
}

template <typename T, typename K>
K Graph<T, K>::removeVertex(Vertex<T, K> * v) {
	K value = v->getData();
	std::list<Edge<T, K> *>::iterator it;
	for (it = v->neighbours->begin(); it != v->neighbours->end(); it++) {

		if ((*it)->neighbours1 == v->neighbours)
			(*it)->neighbours2->remove(*it);
		else
			(*it)->neighbours1->remove(*it);

		Edge<T, K> * tmp = *it;
		*it = nullptr;
		delete tmp;
	}
	v->neighbours->clear();
	delete v;

	return value;
}

template <typename T, typename K>
T Graph<T, K>::removeEdge(Edge<T, K> * e) {
	T value = e->getWeight();
	e->neighbours1->remove(e);
	e->neighbours2->remove(e);
	edgesL.remove(e);
	delete e;

	return value;

}