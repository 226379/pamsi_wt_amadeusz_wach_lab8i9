#pragma once
template <typename T, typename K> //waga, wartosc
class Kruskal {
	T * cluster;
	Vertex<T, K> * source;
	int verticesCount;
	Graph<T, K> graph;
	Graph<T, K> result;
	std::priority_queue<Edge<T, K> *> Q;

public:
	Kruskal(Graph<T, K> _graph) {
		graph = _graph;
		verticesCount = graph.vertices().size();
		cluster = new int[verticesCount];
		for (int i = 0; i < verticesCount; i++)
			cluster[graph.vertices()[i]->getID()] = graph.vertices()[i]->getID();
		std::list<Edge<T, K> *>::iterator it;
		for (it = graph.edges().begin(); it != graph.edges().end(); it++) {
			Q.push(*it);
		}
	}
	~Kruskal() {
		//delete[] cluster;
	}
	void launch() {
		while(result.vertices().size() < graph.vertices().size()-1){
			if (Q.size() == 0) break;
			Edge<T, K> * e = Q.top();
			Q.pop();
			Vertex<T, K> * u = e->getV1();
			Vertex<T, K> * v = e->getV2();
			if (cluster[u->getID()] != cluster[v->getID()]) {
				Vertex<T, K> * _u = result.insertVertex(u->getData());
				Vertex<T, K> * _v = result.insertVertex(v->getData());
				result.insertEdge(_u, _v, e->getWeight());
			
				cluster[v->getID()] = cluster[u->getID()];
			}
		}
	}
};
