#pragma once
template <typename typ>
class KolejkaPriorytetowawsk {
public:
	KolejkaPriorytetowawsk() {
		next = 0;
	}
	typ element;
	int priorytet;
	class KolejkaPriorytetowawsk<typ> *next;
	KolejkaPriorytetowawsk(typ el, KolejkaPriorytetowawsk *n = 0) {
		element = el; next = n;;
	}
};
template <typename typ>
class KolejkaPriorytetowa {
public:
	KolejkaPriorytetowa() {
		head = tail = 0;
	}
	int CzyPusta() {
		return head == 0;
	}
	void DodajElement(typ, int p);
	typ UsunMin();
	typ ZwrocElement(int) const;
	void PokazZawartosc() const;
	void UsunWszystko();
private:
	KolejkaPriorytetowawsk<typ> *head, *tail;
};
