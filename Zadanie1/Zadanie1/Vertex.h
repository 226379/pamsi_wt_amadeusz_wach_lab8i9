#pragma once
#include <list>


template <typename T, typename K>
class Edge;

template <typename T, typename K>
class Vertex {
	K data;
	int ID;
public:
	std::list<Edge<T, K> *> * neighbours;
	Vertex() {
		neighbours = new std::list<Edge<T, K> *>;
	}
	Vertex(K _data, int _ID) {
		neighbours = new std::list<Edge<T, K> *>;
		data = _data;
		ID = _ID;
	}
	std::list<Edge<T, K> *> * getNeighbours();
	void setEdge(K _data) {
		data = _data;
	}
	K getData() {
		return data;
	}
	int getID() {
		return ID;
	}
	void setID(int _ID) {
		ID = _ID;
	}
};

template <typename T, typename K>
std::list<Edge<T, K> *> * Vertex<T, K>::getNeighbours() {
	return neighbours;
}