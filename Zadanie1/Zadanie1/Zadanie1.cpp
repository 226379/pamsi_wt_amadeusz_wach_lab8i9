// Zadanie1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <chrono>
#include "Graph.h"
#include "PriorityQueue.h"
#include <queue>
#include "Dijkstra.h"
#include "Prima.h"
#include "Kruskal.h"
#include <fstream>

using namespace std;

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::duration<float> fsec;


int main()
{
	fsec duration;
	srand(time(0));

	ofstream file("wynik.txt");

	int V[] = { 10, 50, 100, 500, 1000 };
	int Vcount = 5;

	float dentisity[] = { 0.25, 0.50, 0.75, 1 };
	int dentisityCount = 4;

	int probes = 100;

	for (int i = 0; i < Vcount; i++) {
		cout << "Liczba wierzcholkow: " << V[i] << endl;
		file << "Liczba wierzcholkow: " << V[i] << endl;
		for (int j = 0; j < dentisityCount; j++) {
			int n = 0;
			float sum = 0.0;

			Graph<int, int> * Graphs = new Graph<int, int>[probes];

			for (int k = 0; k < probes; k++) {
				//generowanie wierzcholkow
				for (int x = 0; x < V[i]; x++)
					Graphs[k].insertVertex(rand()%1000);
				//generowanie krawedzi
				for (int y = 0; y < dentisity[y] * (V[i] * (V[i] - 1)) / 2; y++) {
					Vertex<int, int> * u;
					Vertex<int, int> * v;
					int vSize = Graphs[k].vertices().size();
					do{
						u = (Graphs[k].vertices())[rand() % vSize];
						v = (Graphs[k].vertices())[rand() % vSize];
						//losowanie dwoch wierzcholkow
					} while (Graphs[k].areAdjancent(u,v));
					Graphs[k].insertEdge(u, v, rand() % 1000);
				}

			}

			cout << "\tGestosc: " << dentisity[j] * 100 << "%" << endl;
			cout << "\t\tKruskal" << endl;
			file << "\tGestosc: " << dentisity[j] * 100 << "%" << endl;
			file << "\t\tKruskal" << endl;
			for (int k = 0; k < probes; k++) {
				
				auto start = Time::now();
				//kruskal
				Kruskal<int, int> K(Graphs[k]);
				K.launch();
				auto stop = Time::now();
				duration = stop - start;
				sum += duration.count();
				n++;
			}
			cout << "\t\tLaczny czas: " << sum << "Sredni czas: " << sum/n << endl;
			file << "\t\tLaczny czas: " << sum << "Sredni czas: " << sum / n << endl;

			n = 0;
			sum = 0.0;

			cout << "\t\tPrima" << endl;
			file << "\t\tPrima" << endl;
			for (int k = 0; k < probes; k++) {
				auto start = Time::now();
				//prima
				Prima<int, int> P(Graphs[k], Graphs[k].vertices()[rand() % Graphs[k].vertices().size()]);
				P.launch();
				auto stop = Time::now();
				duration = stop - start;
				sum += duration.count();
				n++;
			}
			cout << "\t\tLaczny czas: " << sum << "Sredni czas: " << sum / n << endl;
			file << "\t\tLaczny czas: " << sum << "Sredni czas: " << sum / n << endl;

			n = 0;
			sum = 0.0;

			cout << "\t\tDijkstra" << endl;
			file << "\t\tDijkstra" << endl;
			for (int k = 0; k < probes; k++) {
				auto start = Time::now();
				//dijkstra
				Dijkstra<int, int> D (Graphs[k], Graphs[k].vertices()[rand() % Graphs[k].vertices().size()]);
				D.launch();
				auto stop = Time::now();
				duration = stop - start;
				sum += duration.count();
				n++;
			}
			cout << "\t\tLaczny czas: " << sum << "Sredni czas: " << sum / n << endl;
			file << "\t\tLaczny czas: " << sum << "Sredni czas: " << sum / n << endl;
		}
	}
	cin.ignore();
	getchar();
    return 0;
}

