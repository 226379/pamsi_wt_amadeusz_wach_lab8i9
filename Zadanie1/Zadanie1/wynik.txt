Liczba wierzcholkow: 10
	Gestosc: 25%
		Kruskal
		Laczny czas: 0.0929608Sredni czas: 0.000929608
		Prima
		Laczny czas: 0.0949346Sredni czas: 0.000949346
		Dijkstra
		Laczny czas: 0.840358Sredni czas: 0.00840358
	Gestosc: 50%
		Kruskal
		Laczny czas: 0.0834476Sredni czas: 0.000834476
		Prima
		Laczny czas: 0.100777Sredni czas: 0.00100777
		Dijkstra
		Laczny czas: 0.889622Sredni czas: 0.00889622
	Gestosc: 75%
		Kruskal
		Laczny czas: 0.0895267Sredni czas: 0.000895267
		Prima
		Laczny czas: 0.0988425Sredni czas: 0.000988425
		Dijkstra
		Laczny czas: 0.84174Sredni czas: 0.0084174
	Gestosc: 100%
		Kruskal
		Laczny czas: 0.0880661Sredni czas: 0.000880661
		Prima
		Laczny czas: 0.0936714Sredni czas: 0.000936714
		Dijkstra
		Laczny czas: 1.03575Sredni czas: 0.0103575
Liczba wierzcholkow: 50
	Gestosc: 25%
		Kruskal
		Laczny czas: 0.234988Sredni czas: 0.00234988
		Prima
		Laczny czas: 0.342712Sredni czas: 0.00342712
		Dijkstra
		Laczny czas: 10.5929Sredni czas: 0.105929
	Gestosc: 50%
		Kruskal
		Laczny czas: 0.20554Sredni czas: 0.0020554
		Prima
		Laczny czas: 0.34137Sredni czas: 0.0034137
		Dijkstra
		Laczny czas: 10.6709Sredni czas: 0.106709
	Gestosc: 75%
		Kruskal
		Laczny czas: 0.218251Sredni czas: 0.00218251
		Prima
		Laczny czas: 0.334699Sredni czas: 0.00334699
		Dijkstra
		Laczny czas: 11.185Sredni czas: 0.11185
	Gestosc: 100%
		Kruskal
		Laczny czas: 0.202224Sredni czas: 0.00202224
		Prima
		Laczny czas: 0.328659Sredni czas: 0.00328659
		Dijkstra
		Laczny czas: 10.7057Sredni czas: 0.107057
Liczba wierzcholkow: 100
	Gestosc: 25%
		Kruskal
		Laczny czas: 0.396909Sredni czas: 0.00396909
		Prima
		Laczny czas: 0.646147Sredni czas: 0.00646147
		Dijkstra
		Laczny czas: 37.8234Sredni czas: 0.378234
	Gestosc: 50%
		Kruskal
		Laczny czas: 0.344962Sredni czas: 0.00344962
		Prima
		Laczny czas: 0.611607Sredni czas: 0.00611607
		Dijkstra
		Laczny czas: 40.0076Sredni czas: 0.400076
	Gestosc: 75%
		Kruskal
		Laczny czas: 0.351751Sredni czas: 0.00351751
		Prima
		Laczny czas: 0.619502Sredni czas: 0.00619502
		Dijkstra
		Laczny czas: 31.2562Sredni czas: 0.312562
	Gestosc: 100%
		Kruskal
		Laczny czas: 0.230172Sredni czas: 0.00230172
		Prima
		Laczny czas: 0.515607Sredni czas: 0.00515607
		Dijkstra
		Laczny czas: 30.2025Sredni czas: 0.302025
Liczba wierzcholkow: 500
	Gestosc: 25%
		Kruskal
		Laczny czas: 1.24572Sredni czas: 0.0124572
		Prima
		Laczny czas: 2.1608Sredni czas: 0.021608
		Dijkstra
		Laczny czas: 655.447Sredni czas: 6.55447
	Gestosc: 50%
		Kruskal
		Laczny czas: 1.3639Sredni czas: 0.013639
		Prima
		Laczny czas: 1.92478Sredni czas: 0.0192478
		Dijkstra
		Laczny czas: 649.401Sredni czas: 6.49401
	Gestosc: 75%
		Kruskal
		Laczny czas: 1.15181Sredni czas: 0.0115181
		Prima
		Laczny czas: 2.43179Sredni czas: 0.0243179
		Dijkstra
		Laczny czas: 649.835Sredni czas: 6.49835
	Gestosc: 100%
		Kruskal
		Laczny czas: 1.2231Sredni czas: 0.012231
		Prima
		Laczny czas: 2.62118Sredni czas: 0.0262118
		Dijkstra
		Laczny czas: 660.043Sredni czas: 6.60043
Liczba wierzcholkow: 1000
	Gestosc: 25%
		Kruskal
		Laczny czas: 2.5894Sredni czas: 0.025894
		Prima
		Laczny czas: 5.03868Sredni czas: 0.0503868
		Dijkstra
		Laczny czas: 2376.29Sredni czas: 23.7629
	Gestosc: 50%
		Kruskal
		Laczny czas: 1.85874Sredni czas: 0.0185874
		Prima
		Laczny czas: 3.76955Sredni czas: 0.0376955
		Dijkstra
		Laczny czas: 2411.73Sredni czas: 24.1173
	Gestosc: 75%
		Kruskal
		Laczny czas: 1.90189Sredni czas: 0.0190189
		Prima
		Laczny czas: 3.64316Sredni czas: 0.0364316
		Dijkstra
		Laczny czas: 2177.82Sredni czas: 21.7782
	Gestosc: 100%
		Kruskal
		Laczny czas: 2.08272Sredni czas: 0.0208272
		Prima
		Laczny czas: 3.63333Sredni czas: 0.0363333
		Dijkstra
		Laczny czas: 2142.41Sredni czas: 21.4241
